﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CinemaMovie.Models;
using Microsoft.EntityFrameworkCore;

namespace CinemaMovie.Controllers
{
    public class HomeController : Controller
    {
        private MoviesContext db;
        public HomeController(MoviesContext context)
        {
           db  = context;
        }
        public IActionResult Index()
        {
            ViewBag.movies = db.Movies.ToList();
            return View();
        }
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public string Add(Movie movie)
        {
            // добавляем информацию в базу данных
            db.Movies.Add(movie);
            // сохраняем в бд все изменения
            db.SaveChanges();
            return "---Спасибо---";
        }
        [HttpGet] // метод редактирования
        public async Task<IActionResult> Edit(int? id) 
        {
            if (id != null)
            {
                Movie movie = await db.Movies.FirstOrDefaultAsync(p => p.Id == id);
                if (movie != null)
                    return View(movie);
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Movie movie)
        {
            db.Movies.Update(movie);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [HttpGet] // метод удаления
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? id) 
        {
            if (id != null)
            {
                Movie movie = await db.Movies.FirstOrDefaultAsync(p => p.Id == id);
                if (movie != null)
                    return View(movie);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id != null)
            {
                Movie movie = await db.Movies.FirstOrDefaultAsync(p => p.Id == id);
                if (movie != null)
                {
                    db.Movies.Remove(movie);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            return NotFound();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
    public class HomeActorController : Controller
    {
        private MoviesContext db;
        public HomeActorController(MoviesContext context)
        {
            db = context;
        }
        [HttpGet]
        public ActionResult AddActor(string Name)
        {
            ViewBag.Actor = Name;
            return View();
        }
        [HttpPost]
        public string AddActor(Actor actor)
        {
            // добавляем информацию в базу данных
            db.Actors.Add(actor);
            // сохраняем в бд все изменения
            db.SaveChanges();
            return "---Спасибо---";
        }
        [HttpGet] // метод редактирования
        public async Task<IActionResult> EditActor(int? id)
        {
            if (id != null)
            {
                Actor actor = await db.Actors.FirstOrDefaultAsync(p => p.Id == id);
                if (actor != null)
                    return View(actor);
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> EditActor(Actor actor)
        {
            db.Actors.Update(actor);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [HttpGet] // метод удаления
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDeleteActor(int? id)
        {
            if (id != null)
            {
                Actor actor = await db.Actors.FirstOrDefaultAsync(p => p.Id == id);
                if (actor != null)
                    return View(actor);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> DeleteActor(int? id)
        {
            if (id != null)
            {
                Actor actor = await db.Actors.FirstOrDefaultAsync(p => p.Id == id);
                if (actor != null)
                {
                    db.Actors.Remove(actor);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            return NotFound();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

}
