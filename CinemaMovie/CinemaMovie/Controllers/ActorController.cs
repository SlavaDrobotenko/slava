﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaMovie.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CinemaMovie.Controllers
{
    public class ActorController : Controller
    {
        private MoviesContext db;
        public ActorController(MoviesContext context)
        {
            db = context;
        }
        // GET: Actor
        public IActionResult Index()
        {
            ViewBag.actors = db.Actors.ToList();
            return View();
        }
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(Actor actor)
        {
            // добавляем информацию в базу данных
            db.Actors.Add(actor);
            // сохраняем в бд все изменения
            db.SaveChanges();
            return Redirect("/Home/Index");
        }
        [HttpGet] // метод редактирования
        public async Task<IActionResult> Edit(int? id)
        {
            if (id != null)
            {
                Actor actor = await db.Actors.FirstOrDefaultAsync(p => p.Id == id);
                if (actor != null)
                    return View(actor);
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Actor actor)
        {
            db.Actors.Update(actor);
            await db.SaveChangesAsync();
            return RedirectToAction("/Home/Index");
        }
        [HttpGet] // метод удаления
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? id)
        {
            if (id != null)
            {
                Actor actor = await db.Actors.FirstOrDefaultAsync(p => p.Id == id);
                if (actor != null)
                    return View(actor);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id != null)
            {
                Actor actor = await db.Actors.FirstOrDefaultAsync(p => p.Id == id);
                if (actor != null)
                {
                    db.Actors.Remove(actor);
                    await db.SaveChangesAsync();
                    return RedirectToAction("/Home/Index");
                }
            }
            return NotFound();
        }
    }
}