﻿using System.Collections.Generic;

namespace CinemaMovie.Models
{
    public class Movie
    {
        public Movie()
        {
            Actors = new List<Actor>();
        }

        public int Id { get; set; }
        public string Name { get; set; }  // название фильма
        public string Genre { get; set; }  //жанр
        public int Year { get; set; }  //год выпуска
        public int Duration { get; set; }  //длительность
        public virtual ICollection<Actor> Actors { get; set; } //ссылка на связную модель Actors

        public Movie(string name, string genre, int year, int duration) // конструктор Movie
        {
            Name = name; Genre = genre; Year = year; Duration = duration;
        }
    }
}
