﻿namespace CinemaMovie.Models
{
    public class Actor
    {
        public int Id { get; set; }
        public string Firstname { get; set; }  // имя актера
        public string Lastname { get; set; }  // фамилия актера
        public Movie Movie { get; set; } // связь один ко многим
        
    } 
}
